import React from "react";
import { Container, Navbar, Nav } from "react-bootstrap";
import { Link } from "react-router-dom";
import "./header.css";

const Header = () => {
  return (
    <div>
      <Container fluid className="nav-color">
        <Navbar expand="lg" className="navbar">
          <Navbar.Brand className="">
            <Link to="/">
              <div className="logo">DICAT DEV</div>
            </Link>
          </Navbar.Brand>
          <Navbar.Toggle
            aria-controls="basic-navbar-nav"
            className="custom-toggler"
          />

          <Navbar.Collapse id="basic-navbar-nav">
            <Nav className="ms-auto">
              <Nav.Link className="link">
                <Link to="/"> Accueil</Link>
              </Nav.Link>
              <Nav.Link className="link">
                <Link to="/qui-suis-je?">Qui Suis-Je ?</Link>
              </Nav.Link>
              <Nav.Link className="link">
                <Link to="/competences">Compétences</Link>
              </Nav.Link>

              <Nav.Link className="link">
                <Link to="/projets">Projets</Link>
              </Nav.Link>
              <Nav.Link className="link">
                <Link to="/contact">Me Contacter</Link>
              </Nav.Link>
            </Nav>
          </Navbar.Collapse>
        </Navbar>
      </Container>
    </div>
  );
};

export default Header;
