import React from "react";
import Header from "../header/Header";
import { Route, Routes } from "react-router-dom";
import Accueil from "../pages/accueil/Accueil";
import QuiSuisJe from "../pages/qui-suis-je/QuiSuisJe";
import Competences from "../pages/competences/Competences";
import Contact from "../pages/contact/contact";

import Projets from "../pages/projets/projets";
import "./layout.css";

const Layout = () => {
  return (
    <div>
      <Header />
      <Routes>
        <Route path="/" element={<Accueil />} />
        <Route path="/qui-suis-je?" element={<QuiSuisJe />} />
        <Route path="/competences" element={<Competences />} />
        <Route path="/projets" element={<Projets />} />
        <Route path="/contact" element={<Contact />} />
      </Routes>
    </div>
  );
};

export default Layout;
