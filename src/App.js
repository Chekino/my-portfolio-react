import Header from "./header/Header";
import Layout from "./layout/Layout";
import "./app.css";

function App() {
  return (
    <div className="App">
      <Layout />
    </div>
  );
}

export default App;
