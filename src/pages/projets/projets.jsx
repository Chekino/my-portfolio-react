import React from "react";
import {
  Box,
  Heading,
  Text,
  Img,
  Flex,
  Center,
  useColorModeValue,
  HStack,
} from "@chakra-ui/react";
import gitlab from "../../assets/gitlab.png";
import ivoryfood from "../../assets/ivory-foods.png";
import portfolioimg from "../../assets/portfolio.png";
import { motion } from "framer-motion";
const Projets = () => {
  return (
    <motion.div
      initial={{ opacity: 0, scale: 0 }}
      animate={{ opacity: 1, scale: 1 }}
      transition={{ duration: 0.5 }}
      className="bg-color"
    >
      <h1 className="pb-5 justify-content-center d-flex">Mes Projets</h1>
      <div className="d-flex flex-wrap justify-content-center">
        <div className="mx-2">
          <Box
            w="xs"
            rounded={"sm"}
            overflow={"hidden"}
            bg="white"
            border={"1px"}
            borderColor="black"
            boxShadow={useColorModeValue("6px 6px 0 black", "6px 6px 0 cyan")}
          >
            <Box h={"150px"} borderBottom={"1px"} borderColor="black">
              <Img
                src={ivoryfood}
                objectFit="cover"
                h="full"
                w="full"
                alt={"Blog Image"}
              />
            </Box>
            <Box p={1}>
              <Heading color={"#0369a1"} fontSize={"2xl"} noOfLines={1}>
                Ivory-Foods
              </Heading>
              <Text color={"gray.700"}>
                <ol>
                  <li> Application Web Restaurant</li>
                  <li>Liste de plats au menu</li>
                  <li>
                    Selectionnez un ou plusieurs plats et passez des commandes
                  </li>
                  <li>Connexion Utilisateurs</li>
                </ol>
              </Text>
              <Text color={"gray.700"}>
                #React Js #Bootstrap #Express Js #Node Js #Mongo DB
              </Text>
            </Box>

            <HStack borderTop={"1px"} color="black">
              <Flex
                p={4}
                alignItems="center"
                justifyContent={"space-between"}
                roundedBottom={"sm"}
                cursor={"pointer"}
                w="full"
              >
                <a href="https://ivory-food.vercel.app/" target="blank">
                  <Text fontSize={"md"} fontWeight={"semibold"}>
                    Visitez le site
                  </Text>
                </a>
              </Flex>
              <Flex
                p={4}
                alignItems="center"
                justifyContent={"space-between"}
                roundedBottom={"sm"}
                borderLeft={"1px"}
                cursor="pointer"
              >
                <a href="https://gitlab.com/Chekino/ivory-food" target="blank">
                  <img src={gitlab} alt="" />
                </a>
              </Flex>
            </HStack>
          </Box>
        </div>
        <div className="mx-2">
          <Box
            w="xs"
            rounded={"sm"}
            overflow={"hidden"}
            bg="white"
            border={"1px"}
            borderColor="black"
            boxShadow={useColorModeValue("6px 6px 0 black", "6px 6px 0 cyan")}
          >
            <Box h={"150px"} borderBottom={"1px"} borderColor="black">
              <Img
                src={portfolioimg}
                roundedTop={"sm"}
                objectFit="cover"
                h="full"
                w="full"
                alt={"Blog Image"}
              />
            </Box>
            <Box p={1}>
              <Heading color={"#0369a1"} fontSize={"2xl"} noOfLines={1}>
                Dev Portfolio
              </Heading>
              <Text color={"gray.700"}>
                <ol>
                  <li>Accueil</li>
                  <li>Qui Suis je ?</li>
                  <li>Compétences</li>
                  <li>Projets</li>
                  <li>Me Contacter </li>
                </ol>
              </Text>
              <Text color={"gray.700"}>
                #React Js #Bootstrap #Chakra UI #Framer Motion
              </Text>
            </Box>

            <HStack borderTop={"1px"} color="black">
              <Flex
                p={4}
                alignItems="center"
                justifyContent={"space-between"}
                roundedBottom={"sm"}
                cursor={"pointer"}
                w="full"
              >
                <a href="https://dicat-dev.vercel.app/" target="blank">
                  <Text fontSize={"md"} fontWeight={"semibold"}>
                    Visitez le site
                  </Text>
                </a>
              </Flex>
              <Flex
                p={4}
                alignItems="center"
                justifyContent={"space-between"}
                roundedBottom={"sm"}
                borderLeft={"1px"}
                cursor="pointer"
              >
                <a
                  href="https://gitlab.com/Chekino/my-portfolio-react"
                  target="blank"
                >
                  <img src={gitlab} alt="" />
                </a>
              </Flex>
            </HStack>
          </Box>
        </div>
      </div>
    </motion.div>
  );
};

export default Projets;
