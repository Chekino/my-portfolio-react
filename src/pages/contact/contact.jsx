import React, { useRef } from "react";

import {
  MDBInput,
  MDBCheckbox,
  MDBBtn,
  MDBCol,
  MDBRow,
  MDBTextArea,
} from "mdb-react-ui-kit";
import { motion } from "framer-motion";
import emailjs from "@emailjs/browser";
import { ToastContainer, toast } from "react-toastify";

export default function Contact() {
  const form = useRef();

  const sendEmail = (e) => {
    e.preventDefault();

    emailjs
      .sendForm(
        "service_xlcqdxn",
        "template_rd9i3tu",
        form.current,
        "TG-1yw9UWYvJtaOBb"
      )
      .then(
        (result) => {
          console.log(result.text);
          toast.success("Formulaire envoyé avec succès");
        },
        (error) => {
          console.log(error.text);
        }
      );
  };

  return (
    <motion.div // Enveloppez votre contenu avec motion.div
      initial={{ opacity: 0, scale: 0 }}
      animate={{ opacity: 1, scale: 1 }}
      transition={{ duration: 0.5 }}
      className="bg-color"
    >
      <h1 className="pb-5 justify-content-center d-flex">Me Contacter</h1>
      <form className="container" ref={form} onSubmit={sendEmail}>
        <MDBCol>
          <MDBInput
            label={<span className="text-black">Nom et prenom</span>}
            name="user_name"
            className="text-white mb-2"
            type="text"
            required
          />
        </MDBCol>
        <MDBCol>
          <MDBInput
            type="email"
            label={<span className="text-black">Email</span>}
            className="text-white mb-2"
            name="user_email"
            required
          />
        </MDBCol>
        <MDBCol>
          <MDBInput
            type="number"
            label={<span className="text-black">Numero de téléphone</span>}
            name="user_number"
            className="text-white mb-4"
            required
          />
        </MDBCol>

        <MDBTextArea
          wrapperClass="mb-4"
          type="text"
          textarea
          rows={4}
          label={<span className="text-black">Message</span>}
          className="text-white "
          name="message"
          required
        />

        <MDBBtn type="submit" className="mb-4" block>
          Envoyer le message
        </MDBBtn>
      </form>
      <ToastContainer />
    </motion.div>
  );
}
