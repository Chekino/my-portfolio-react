import React from "react";
import Card from "react-bootstrap/Card";
import moi from "../../assets/moi.jpeg";
import { motion } from "framer-motion";

const QuiSuisJe = () => {
  return (
    <motion.div
      initial={{ opacity: 0, scale: 0 }}
      animate={{ opacity: 1, scale: 1 }}
      transition={{ duration: 0.5 }}
      className="bg-color"
    >
      <div className="container ">
        <h1 className="pb-5 justify-content-center d-flex">Qui Suis-Je ?</h1>
        <div className="row">
          <div className="col">
            <h4 className="pb-4">
              Je suis un passionné d'informatique et en particulier du
              developpemt web. Actuellement, je poursuis mes études à
              l'Université Virtuelle de Côte d'Ivoire, où je me concentre sur
              l'option "Développeur d'Applications et E-Services".
            </h4>
            <strong>
              Depuis que j'ai entamé ce parcours académique, j'ai acquis des
              compétences solides en programmation, en conception de sites web
              et en développement d'applications. Mon intérêt pour la
              technologie m'a poussé à explorer divers langages de
              programmation, des frameworks web modernes et des outils
              essentiels pour créer des solutions numériques efficaces et
              conviviales en suivant un Bootcamp Javascript Full-stack auprès
              d'une structure de formation nommée GOMYCODE. Mon objectif en tant
              que développeur web full-stack est de concevoir des produits
              innovants et fonctionnels qui améliorent la vie des utilisateurs.
              J'aime travailler sur des projets variés, de la création de sites
              web dynamiques à la mise en place d'applications interactives. Mon
              expérience en tant qu'étudiant en informatique m'a également
              appris l'importance de l'apprentissage continu dans un domaine en
              constante évolution. En dehors de ma passion pour la technologie,
              j'aime relever de nouveaux défis, résoudre des problèmes complexes
              et collaborer avec d'autres professionnels pour atteindre des
              objectifs communs. N'hésitez pas à me contacter si vous souhaitez
              en savoir plus ou discuter de futures opportunités de
              collaboration
            </strong>
          </div>
          <div className="col d-flex justify-content-center p-2">
            <card style={{ width: "20rem" }}>
              <Card.Img src={moi} />
            </card>
          </div>
        </div>
      </div>
    </motion.div>
  );
};

export default QuiSuisJe;
