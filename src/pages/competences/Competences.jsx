import React from "react";
import css from "../../assets/css.png";
import html from "../../assets/html.png";
import js from "../../assets/javascript.png";
import bootstrap from "../../assets/bootstrap.png";
import react from "../../assets/react.png";
import chakra from "../../assets/chakra.png";
import node from "../../assets/node.png";
import mongo from "../../assets/mongo.png";
import express from "../../assets/express.png";
import git from "../../assets/git.png";
import github from "../../assets/github.png";
import gitlab from "../../assets/gitlab.png";
import { motion } from "framer-motion";

const Competences = () => {
  return (
    <motion.div
      initial={{ opacity: 0, scale: 0 }}
      animate={{ opacity: 1, scale: 1 }}
      transition={{ duration: 0.5 }}
      className="bg-color"
    >
      <div className="container">
        <h1 className="pb-4 text-center">Competences</h1>
        <h4 className="text-center">
          J'aime apprendre de nouvelles choses et expérimenter de nouvelles
          technologies.
          <p>
            Voici quelques-uns des principaux langages, technologies, outils et
            plates-formes avec lesquels j'ai travaillé :
          </p>
        </h4>
        <div className="">
          <h3 className="text-center">Developpement Frontend</h3>
          <div className="d-flex flex-wrap text-center justify-content-center">
            <div className="mx-2">
              <img src={html} alt="" />
              <p>HTML</p>
            </div>
            <div className="mx-2">
              <img src={css} alt="" />
              <p>CSS</p>
            </div>
            <div className="mx-2">
              <img src={bootstrap} alt="" />
              <p>Bootstrap</p>
            </div>
            <div className="mx-2">
              <img src={js} alt="" />
              <p>JavaScript</p>
            </div>
            <div className="mx-2">
              <img src={react} alt="" />
              <p>React</p>
            </div>
            <div className="mx-2">
              <img src={chakra} alt="" />
              <p>Chakra UI</p>
            </div>
          </div>
        </div>
        <div>
          <h3 className="text-center">Developpement Backend</h3>
          <div className="d-flex flex-wrap text-center justify-content-center">
            <div className="mx-2">
              <img src={node} alt="" />
              <p>Node Js</p>
            </div>
            <div className="mx-2">
              <img src={express} alt="" />
              <p>Express Js</p>
            </div>
            <div className="mx-2">
              <img src={mongo} alt="" />
              <p>Mongo DB</p>
            </div>
          </div>
        </div>
        <div>
          <h3 className="text-center"> Outils de Gestion de Version</h3>
          <div className="d-flex flex-wrap text-center justify-content-center">
            <div className="mx-2">
              <img src={git} alt="" />
              <p>Git </p>
            </div>
            <div className="mx-2">
              <img src={github} alt="" />
              <p>Github</p>
            </div>
            <div className="mx-2">
              <img src={gitlab} alt="" />
              <p>GitLab</p>
            </div>
          </div>
        </div>
      </div>
    </motion.div>
  );
};

export default Competences;
