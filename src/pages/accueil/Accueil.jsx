import React from "react";
import "./accueil.css";
import { useState, useEffect } from "react";
import { motion } from "framer-motion";
import LinkedInIcon from "@mui/icons-material/LinkedIn";
import GitHubIcon from "@mui/icons-material/GitHub";
import ContactMailIcon from "@mui/icons-material/ContactMail";
import Typewriter from "typewriter-effect";

const Accueil = () => {
  const texts = ["Full-stack", "Frontend", "Backend"];
  const colors = ["#f97316", "#84cc16", "#1f2937"];
  const [textIndex, setTextIndex] = useState(0);

  useEffect(() => {
    const interval = setInterval(() => {
      setTextIndex((prevIndex) => (prevIndex + 1) % texts.length);
    }, 2000);
    return () => {
      clearInterval(interval);
    };
  }, []);
  return (
    <motion.div // Enveloppez votre contenu avec motion.div
      initial={{ opacity: 0, scale: 0 }}
      animate={{ opacity: 1, scale: 1 }}
      transition={{ duration: 0.5 }}
      className="bg-color d-flex justify-content-center align-items-center "
    >
      <div className=" text-center ">
        <div className="">
          <div className="">
            <h1 className="mb-3">Diakite Cheikh Ahmad </h1>
            <div>
              <h4 className="mb-3 col">
                Je suis Developpeur web
                <span className="" style={{ color: colors[textIndex] }}>
                  <Typewriter
                    options={{
                      autoStart: true,
                      loop: true,
                      delay: 80,
                      strings: ["Full-stack", "Frontend", "Backend"],
                    }}
                  />
                </span>
              </h4>
            </div>

            <div>
              <a
                className="btn btn-light btn-lg m-2"
                href="https://ci.linkedin.com/in/cheikh-ahmad-tidjani-diakite-3693a1280?trk=people-guest_people_search-card"
                role="button"
                target="blank"
              >
                <LinkedInIcon />
              </a>
              <a
                className="btn btn-light btn-lg m-2"
                href="https://gitlab.com/Chekino"
                role="button"
                target="blank"
              >
                <GitHubIcon />
              </a>
              <a
                className="btn btn-light btn-lg m-2"
                href="mailto:dicatdev@gmail.com"
                role="button"
              >
                <ContactMailIcon />
              </a>
            </div>
          </div>
        </div>
      </div>
    </motion.div>
  );
};

export default Accueil;
